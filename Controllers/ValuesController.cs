﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using EFCore.Dominio;
using EFCore.Repo;

namespace EFCore.Netcore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        public readonly HeroiContext _context;
        public ValuesController(HeroiContext context)
        {
            _context = context;
        }

        [HttpGet("{id}")]
        public ActionResult Delete(int id)
        {
            var heroi = _context.Herois.Where(h => h.Id == id).FirstOrDefault();
            _context.Herois.Remove(heroi);

            _context.SaveChanges();

            return Ok();
        }

        [HttpGet("addRange")]
        public ActionResult GetAddRange(string filtro)
        {
            _context.AddRange(
                new Heroi { Nome = "Scooby" },
                new Heroi { Nome = "Doutor Loko" },
                new Heroi { Nome = "Capitão Brazuka" },
                new Heroi { Nome = "Pretinha" },
                new Heroi { Nome = "viúva do Iphone" }
                );

            _context.SaveChanges();

            return Ok();
        }

        [HttpGet("atualizar/{filtro}")]
        public ActionResult GetAtualizar(string filtro)
        {
            var heroi = _context.Herois
                .Where(h => h.Id == 1)
                .FirstOrDefault();

            heroi.Nome = "Matador de Onça";
            _context.SaveChanges();

            return Ok();
        }

        [HttpGet("filtro/{filtro}")]
        public ActionResult Get(string filtro)
        {
            var listHerois = _context.Herois
                .Where(h => h.Nome.Contains(filtro))
                .ToList();
            return Ok(listHerois);
        }

        // GET api/values
        [HttpGet]
        public ActionResult Get()
        {
            var listHerois = _context.Herois.ToList();
            return Ok(listHerois);
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            var heroi = new Heroi {
            Nome = "Matado de onça"
            };
             
            
            _context.Herois.Add(heroi);
            _context.SaveChanges();
            
            return Ok();
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }
    }
}
