﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EFCore.Dominio;
using EFCore.Repo;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EFCore.Netcore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class HeroiController : ControllerBase
    {
        private readonly HeroiContext _context;

        public HeroiController(HeroiContext context)
        {
            _context = context;
        }
        // GET: api/<HeroiController>
        [HttpGet("{id}")]
        public ActionResult Get(int id)
        {
            try
            {
                var model = _context.Herois.FirstOrDefault(Heroi => Heroi.Id == id);
                return Ok(model);
            }
            catch (Exception ex)
            {

                return BadRequest($"Error no sistema: {ex}");
            }
            
        }

        // POST api/<HeroiController>
        [HttpPost]
        public ActionResult Post(Heroi model)
        {
            try
            {
                _context.Herois.Add(model);
                _context.SaveChanges();
                return Ok("Bazinga");
            }
            catch (Exception ex)
            {

                return BadRequest($"Error no sistema: {ex}");
            }
        }

        // PUT api/<HeroiController>/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, Heroi heroi)
        {
            try
            {
                if (_context.Herois.AsNoTracking().FirstOrDefault(h => h.Id == id)  != null)
                {
                    _context.Herois.Update(heroi);
                    _context.SaveChanges();
                    return Ok("Bazinga");
                }
                return Ok("Error: Heroi não foi encontrado");
                
            }
            catch (Exception ex)
            {

                return BadRequest($"Error no sistema: {ex}");
            }
        }

        // DELETE api/<HeroiController>/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
