﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using EFCore.Dominio;
using EFCore.Repo;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace EFCore.Netcore.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class BatalhaController : ControllerBase
    {
        private readonly IEFCoreRepository _repo;

        public BatalhaController(IEFCoreRepository repo)
        {
            _repo = repo;
        }
        // GET: api/<BatalhaController>
        [HttpGet]
        public async Task<IActionResult>  Get()
        {
            try
            {
               var herois = await _repo.GetAllBatalhas();
               return Ok(herois);
            }
            catch (Exception err)
            {

                return BadRequest(error: err);
            }
        }

        // GET api/<BatalhaController>/5
        [HttpGet("{id}", Name ="GetBatalha")]
        public async Task<IActionResult> Get(int id)
        {
            try
            {
                var heroi = await _repo.GetBatalhaById(id, true);
                if (heroi != null)
                {
                    return Ok(heroi);
                }
                return Ok("heroi não encontrado");
            }
            catch (Exception err)
            {

                return BadRequest(error: err);
            }
        }

        // POST api/<BatalhaController>
        [HttpPost]
        public async Task<IActionResult> Post(Batalha batalha)
        {
            try
            {
                _repo.Add(batalha);

                if (await _repo.SaveChangeAsync())
                {
                    return Ok("bazinga");
                }
                return Ok($"Batalha não pode ser cadastrada.");

            }
            catch (Exception err)
            {
                return BadRequest(error: err);
            }
        }

        // PUT api/<BatalhaController>/5
        [HttpPut("{id}")]
        public async Task<IActionResult> Put(int id, Batalha model)
        {
            try
            {
                var batalha = _repo.GetBatalhaById(id);
                if(batalha != null)
                {
                    _repo.Update(batalha);
                    if (await _repo.SaveChangeAsync())
                    {
                        return Ok("bazinga");
                    }
                    return Ok($"Batalha Não pode ser cadastrada.");
                }
                return Ok("Batalha não encontrada");
            }
            catch (Exception err)
            {
                return BadRequest(error: err);
            }
        }

        // DELETE api/<BatalhaController>/5
        [HttpDelete("{id}")]
        public async Task<IActionResult> Delete(int id)
        {
            try
            {
                var batalha = await _repo.GetBatalhaById(id);
                if (batalha != null)
                {
                    _repo.Delete(batalha);
                    if (await _repo.SaveChangeAsync())
                    {
                        return Ok("bazinga");
                    }
                }
                return Ok("Heroi não encontrado");
            }
            catch (Exception err)
            {
                return BadRequest(error: err);
            }
        }
        [HttpDelete("{id}")]
        public async Task<IActionResult> DeleteHeroiOfBatalha(int idBatalha, int idHeroi)
        {
            //try
            //{
            //    var heroi = await _repo.GetBatalhaByHeroi(idBatalha, idHeroi);

            //    if (heroi != null)
            //    {
            //        _repo.Delete(heroi);
            //        if (await _repo.SaveChangeAsync())
            //        {
            //            return Ok("bazinga");
            //        }
            //    }
            //    return Ok("Heroi não encontrado");
            //}
            //catch (Exception err)
            //{
            //    return BadRequest(error: err);
            //}
            return Ok();

        }
    }
}
